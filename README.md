# N-Dimension interview challenge

Every day N-Dimension collects telemetry from hundreds of sensors at installations across North America; we spend a great deal of time "tuning" to minimize how much telemetry we have to collect, and the median daily event-count is still ~350,000. To review every raw alert each day, an analyst would have to cover 10 alerts a second, assuming an 8-hour work day with no water breaks; don't blink!

## Turn data in to information

The analysts need your help! Included in this project is telemetry from a customer sensor which needs analyzing. Here's what you need to accomplish:

1. Produce a datatable sorted by alert_count_sum in descending order
2. In the communication_pairs column, display max 10 pairs, sorted by alert_count_sum in descending order 
3. Provide the ability to filter rows by IP address. You should be able to include AND exclude results.
4. Filter (include and exclude) by snort_rule_id. An sid has the format gid:snort_rule_id:revision
5. Display a count of the rows in the data table

You'll need to load sensor_data.json

The alert data is in this format:
```
[
  {
    "sid": String,
    "message": String,
    "category": String,
    "priority": Number,
    "host": String,
    "alert_count_sum": Number,
    "communication_pairs": [
      {"source_ip": String, "dest_ip": String, "alert_count_sum": Number},
      ...
    ]
  },
  ...
]
```

## Submit your code

Ideally we'd like you to send us a link to a private repo we can clone and run, but a zip file would be acceptable. 

Make sure to include a readme with any instructions. Please do include notes in the readme about your design decisions, constraints, etc!

Please package your solution up as a docker container for us to run.

Most important to us: please, don't spend a lot of time on this assignment. We're all busy. Ideally, we'd like to see what you can accomplish in 1-3 hours.